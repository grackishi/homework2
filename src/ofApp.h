#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		
	// Making sphere array of 1000 
		ofSpherePrimitive orbs[1000];
		ofEasyCam cam;
		ofLight light1;
		ofLight light2;

	// Creates variable to hold noise
		float noiseTime;

	// Variables to hold the x,y,z pos of each orb
		int positionX[1000];
		int positionY[1000];
		int positionZ[1000];

	// Holds radius of each orb
		float rad[1000];


	//GUI element
		ofxPanel gui;
		ofxIntSlider speedSlide;
		ofxToggle lightOn;
		ofxFloatSlider noiseSlide;


		
};
