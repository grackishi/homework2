#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

	// Setup GUI
	gui.setup();
	gui.add(speedSlide.setup("Orb Speed", 50, 0, 200));
	gui.add(lightOn.setup("Lights On", true));
	gui.add(noiseSlide.setup("Noise Speed", 0.1, 0.0, 1.0));

	// Set background to black
	ofSetBackgroundColor(0, 0, 0);
	
	// For loops 
	for (int x = 0; x < 10; x++) {
		for (int y = 0; y < 10; y++) {
			for (int z = 0; z < 10; z++) {
				int currOrb = ((x * 10) + y) * 10 + z;

				// Sets initial radius of each orb
				rad[currOrb] = ofRandom(10);

				// Sets initial position of each orb
				positionX[currOrb] = ofRandom(400);
				positionY[currOrb] = ofRandom(400);
				positionZ[currOrb] = ofRandom(400);

				orbs[currOrb].setPosition(positionX[currOrb], positionY[currOrb], positionZ[currOrb]);
				orbs[currOrb].setRadius(rad[currOrb]);

			}
		}
	}

	// Set up red light
	light1.setDiffuseColor(ofColor(255, 0, 0));
	light1.setPosition(-100, 100, 500);
	light1.enable();

	// Set up blue light
	light2.setDiffuseColor(ofColor(0, 0, 255));
	light2.setPosition(-100, 100, -500);
	light2.enable();

	
}

//--------------------------------------------------------------
void ofApp::update(){

	// Increments noise variable with gui
	noiseTime += noiseSlide;

	// Enables/disables light with gui
	if (lightOn) {
		light1.enable();
		light2.enable();
	}
	else {
		light2.disable();
		light1.disable();
	}

	// For Loops
	for (int x = 0; x < 10; x++) {
		for (int y = 0; y < 10; y++) {
			for (int z = 0; z < 10; z++) {
				// Determines current orb
				int currOrb = ((x * 10) + y) * 10 + z;

				// Moves orb position so they look like they're falling
				positionX[currOrb] -= speedSlide/100;
				positionY[currOrb] -= speedSlide/100;
				positionZ[currOrb] -= speedSlide/100;

				// If the positions are less than 0, reset the orb to 400
				if (positionX[currOrb] <= 0) positionX[currOrb] = 400;
				if (positionY[currOrb] <= 0) positionY[currOrb] = 400;
				if (positionZ[currOrb] <= 0) positionZ[currOrb] = 400;

				orbs[currOrb].setPosition(positionX[currOrb], positionY[currOrb], positionZ[currOrb]);
			
				// Get noise
				float myNoise = 20.0 * ofSignedNoise(x / 5.0, y / 5.0, z / 5.0, noiseTime);

				if (myNoise < 0) myNoise = 0;

				// Set radius of orbs according to noise
				orbs[currOrb].setRadius(myNoise);

			}
		}
	}
}

//--------------------------------------------------------------
void ofApp::draw() {

	ofEnableDepthTest();

	// Setup camera
	cam.begin();
	
	// Move camera
	ofTranslate(-100, -100, -100);
	ofRotate(45,0, 1, 0);
	
	// Draw each orb
	for (int i = 0; i < 1000; i++) {
		orbs[i].draw();
	}
	cam.end();
	
	ofDisableDepthTest();

	cam.draw();

	gui.draw();
}

