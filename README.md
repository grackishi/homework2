# Homework2

![](/screenshot.jpg)

This project uses the voxel noise project we created in class. The spheres now change size based on the noise provided and slowly
drift down in the 3D space. There are also red and blue lights surrounding the cube of spheres. There is also a UI that allows
you to change the rate at which the spheres change size, how fast they move, and if the lights are on or off.

